const btns = document.querySelectorAll('.btn');

document.addEventListener('keydown', (ev) => {

    btns.forEach(item => {       
        item.style.backgroundColor = 'black';
        if (ev.key.toLowerCase() == item.innerText.toLowerCase()) {
            item.style.backgroundColor = 'blue';
        } 
    })   

})